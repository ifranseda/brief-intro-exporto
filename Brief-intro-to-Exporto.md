# How UI Designers get along with Mobile App Developer in HappyFresh

![](image-nobita-giant.png)

I remember sometime around 2012 when first time initiated my first-ever iOS project with [Gage Batubara](https://medium.com/@gage) to create an easy and beautiful app to show time listing, basically prayer times, for people who are heavy mobile users to see when is the break-fasting time in the area. Gage who is a UX/UI designer came up with the idea of the similar apps available in the market back then and try figure out the easiest way to his question: "*What time is break-fasting today?*", and the most important part of all was the momentum of executing this idea as soon as possible as we were approaching fasting month, so we invited some of our colleagues from [Agrisoft](https://github.com/agrisoft) to join us and rushed the development of the app, and then we called it as [Romadon](https://itunes.apple.com/us/app/romadon/id547604486?mt=8).

![Romadon app design](image-romadonapp-460x0w.png)

![Romadon app design](image-romadonapp-628x0w.png)

For us, this project was more of a kind experiment as we were both iPhone users and really love experiment with apps, and also never had a good experience on the platform. I never get excited when I saw the results on my iPhone 4s, it really was amazing how fonts, colors, images just looks wonderful in the iPhone retina display compare with what appeared on iPhone 3GS.

Long story-short, if you noticed all the icons appeared in the screenshots above, the moon, compass, setting gear, location pin, etc. Those were something that frequently modified during the development process, and that was not even a problem for me back then since we were sitting next to each other working intensively as fasting month is about to start within a week. Every assets such as images, colors, fonts, sounds was easy to deliver, easily integrate into the project. Just copy the files, pass the flash-disk, or use File sharing, (or AirDrop?), we can just easily shout at each other asking for assets or progress on this and that.

![Resources directory in Xcode 4 project](image-20180406210221195.png)

Long after [Romadon](https://itunes.apple.com/us/app/romadon/id547604486?mt=8) app was release and eventually become some kind of zombie in AppStore, I was involved in several iOS and Android projects. Something related with assets, icons, fonts, and colors especially when it comes to integrating into the projects is getting more and more complicated as the scope of development is growing bigger and bigger and it also required a lot more assets and also perhaps the UI designer is no longer sitting next to you, or another UI designer is bugged by the clients requesting UI changes which perhaps will lead to send the assets more frequently compared with other. These are something that could slow down the developer when they need to focus on the code rather than integrating assets into projects, even sometimes it will required several Skype call to settle things down as maybe another UI designer created some icons without following any naming conventions or lack of consistency or even worse exported exactly the same image into several files with different names.

As a mobile developer, maybe you can imagine when one of your UI designer sitting in the office received some briefs about what the next feature should look and start working on it, while other designers somewhere working remotely creating set of icons that perhaps never been posted to you, but when both designer finally send the assets to you everything just broken caused by some inconsistencies from designers assets, and then started complaining about it to your designers, and they just shouted back at you.

> "Hey, you're the mobile app developer. You're the one who knows how it should behave in your code, not us. So, start fix it instead"
>

Sometime the situation is worse than it sounds. But, one thing for sure that is sacrificed is the product itself regardless the perspective of designer or developer. Why don't designer set some standard checklist before start deliver the assets? Why developers has to take care of designers assets? Why can't designers maintain their own outputs such as icons, images, fonts, color collection and even naming of icon and color they created?

Should designers have some level of consistency on what will be delivered will ease developer to download the asset and put them into mobile app projects, so there will be no conflicting preferences between developer with Java background setting an icon name as "**iconDropdownButton.png**" while other developers who set some icons with name "**icn_arrow_up.png**", "**btn_icon_arrow_left.png**", **"icn_imageForgotPassword.png**", etc, or allowing developers to set name of hexadecimal colors with "**green_navbar_background_color**", "**color_gandalf**", "**lightOrange**", or else.

Not to mention how often designers will send the updated revision of assets without never really follows developers conventions, this will impose developer to do the same routine renaming all of them over and over again before import them into Xcode and Android studio. This defects in our workflow sometimes may lead our recent app build staged without most recent assets from designer. Even-though we add another step in our sprint require designer in charge to review the design and assets, but that is not enough to solve the issue as it not the root cause of the problem itself.

## How we settle things up in HappyFresh

During the first days development of HappyFresh with less than 7 people and only one designer, the simplest way to deliver assets was by dropping them through Slack channel or attached them in particular JIRA story once designer was done. Sometimes things went out of control when designer started the same sprint working the same feature as developers when visual assets which already sent needs to replaced with updated ones, and even worse when it already compiled for staging for testing has to be recompiled because some of the icons are not using the latest revision. This maybe not always designers fault, sometimes developers are just missed latest assets from designer to compile the project just because there is lot of changes and no idea how to track which one is the latest revision, or maybe there is some defects in the product development process itself which causing this kind of problem?

Then there was a moment when [Zeplin](zeplin.io) were designers favorites and very useful tool for developer to download the assets and inspect design specifications. But the more important part is that it gives designers a simple way to deliver the assets to developers directly from Sketch, something that no tools ever provide before. Even though there are many Sketch plugins that provide easier tools to export all the assets by several clicks, still Zeplin provides something that Slack or JIRA story board cannot provide, of course, because they were not meant to do such things.

![Zeplin](image-20180408221820184.png)



Looks very nice, huh? So, what's the problem with that?

Do you notice how many icons the app have? Who suppose to know where to find other assets required for the app? Do you notice any platform-specific assets required for the app? Does it really necessary to post both assets for iOS and Android to different spec screen? Don't you think it will be some kind of puzzling for developer to download all assets required for the app?

As a developer, I like using [Zeplin](zeplin.io) to inspect the design specs like the size of buttons, how big is the padding between text "Categories" with the text below, what is the margin between the Categories box with the Best Deals box. It really powerful tools to do such things. It extract every layers from Sketch artboard into pieces, even I can see the dimension of the "battery" and its "current battery level" icons in the status bar. Sometimes it feels just irrelevant to have such capability to inspect such element in design specs, as we do not really have feature related with the remaining battery level, or signal strength, or phone system clock.

But one thing that I found a bit annoying is that I have to browse all the screens, check all the assets name one by one to match with what I already have in the app. Not to mention some screens that designers still haven't come up with a decision yet to upload, or one to two screens that is just missing as the designer who were in charged already gone and never hand-over the working files. And also, I'll have to check for another platform whether all the assets are equally completed. 

### Let's make assets happier

The idea is to hand over the responsibility of managing assets back to designers team. Having enough psychological background or rich contexts on why such image need to have certain compositions will brings more proper message to users, as well as choosing a specific RGBA color that reflects more about product value. It will give them resourceful sets to choose proper name for assets such icons, color, font style. These are some of the reason we decided to actually move the responsibility handling all this back to designer's hand.

![HappyFresh designer assets structure](image-20180409000805258.png)

But, it actually more than just set naming convention and rules for structuring the assets. In the other hand, it actually provides centralized design library which shared among all designers in HappyFresh who eventually will add, remove, or modify the assets without losing track of the changes and keeps everyone notified with the recent updates. You might need to open Google Translate to read more about [how useful the design library](https://medium.com/happytech/desain-libarary-menjadi-sangat-penting-ketika-d2a4aedfa302) is even for a new junior designer like [Rahadian Fajar](https://medium.com/@rahadianfajariskandar).

It has been more than 6 months since we initiated this project named **HappyAssets** to improve our design and developer workflow, especially to provide easier way for designers to deliver their outputs, and the results is delightful. No more designers are using Zeplin, Slack, or even JIRA story to deliver their design assets to mobile developer. They're now move even faster than ever before delivering the assets without missing any single detail. Enhance this sophistication, [they are using Git to do all this](https://medium.com/happytech/git-your-head-up-ae9ee1871ee3).

But, as the designer, why do I need to learn a tool that supposed to used by developer? Meet [Exporto](https://exporto.io), a convenient tools for designer to deliver assets.

![Exporto Sketch plugin](image-20180409222437416.png)

At the first time we sparked the idea to fellow designers, most of them are doubted that they can managed to work with Git, as we all know it require some effort to learn.

But that is the ultimate challenge for us when we started developing this to make this tool powerful and yet working seamlessly without require a lot of effort to learn Git. We really are put a high attention to any details to consider this kind of limitations on designer side who actually is playing main role.

...

Once designer has exported all the assets it will automatically uploaded to cloud repository, and instantly will be available for each platform, and the workflow is now move to developer side to merge the assets into mobile app project. Previously, when developer received the assets they still need to drag and drop the assets into .xcassets directory for iOS or screen-density directories for Android to make them available in the project.

This tool helps developers to cut that process and enable developers to work directly on the feature which require the new assets without doing additional steps. In order to download the latest assets committed by designers, developers only need to run this command in the terminal:

```bash
$> exporto sync <project hash name>
```

\* = project hash name or other available command is available in the dashboard.

![](image-20180411212327719.png)

And that's all. Afterward, it will download both iOS and Android assets into single directory under your project. For iOS, in order to add additional xcassets into project is just simply drag and drop it to Xcode. But for Android there is a bit of simple workaround to do it as described in the [documentation](https://developer.android.com/studio/build/build-variants.html#configure-sourcesets).

```yaml
    sourceSets {
        main {
            java.srcDirs = ['src/v2/java']
            res.srcDirs = [
                    '../happyassets/assets/android',
                    '../happyassets/resources/android'
            ]
            assets.srcDirs = ['src/v2/assets']
        }
    }
```

We eliminated the usage of Zeplin as asset delivery platform. If previously designer use it to send icons, font style, color name to developer, recently they only used Zeplin to hack the integration with HappyAssets by put some comment pin to describe what icon name that is used in particular image that is available.

To provide the more immersive and useful tools for both party to collaborate, we also provide similar tools as Zeplin replacement to inspect the design specifications.

![Design Specification Viewer](image-20180409234743925.png)

Months after, our developers are no longer need to struggle with asset management as previously we encountered as it already been taken care of by this tool, and it really speeds up the development workflow as we cut a lot of process off. Based on this experience to improve our workflow, it opens up other possibility such as delivering translations directly into the mobile app, and etc.

[Exporto](https://exporto.io) eventually become one of the projects run under HappyFresh incubation program to facilitate internal projects to escalate and share with community. We are more than happy to rolling this for public beta, we'll open the free limited access to use this tool. So [sign up](https://exporto.io)  and create your project, and then you can download the [Sketch plugin](Exporto.sketchplugin.zip) along with [Sample sketch design file](Sample.zip) to give you more clearer visibility on how to solve similar workflow issues like we previously had.

So, give it a try. We'd love to explore how this tool could help you.