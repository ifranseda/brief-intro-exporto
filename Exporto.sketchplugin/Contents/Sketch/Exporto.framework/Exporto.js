var loadExporto = function(context) {
    var Exporto_FrameworkPath = Exporto_FrameworkPath || COScript.currentCOScript().env().scriptURL.path().stringByDeletingLastPathComponent();
    var Exporto_Log = Exporto_Log || log;
    
    (function() {
        var mocha = Mocha.sharedRuntime();
        var frameworkName = "Exporto";
        var directory = Exporto_FrameworkPath;
        Exporto_Log("Mocha: " + mocha);
        Exporto_Log("framework directory: " + directory);
        Exporto_Log("mocha.valueForKey(frameworkName) = " + mocha.valueForKey(frameworkName));
        Exporto_Log("[mocha loadFrameworkWithName:frameworkName inDirectory:directory] = " + [mocha loadFrameworkWithName:frameworkName inDirectory:directory]);
        if (mocha.valueForKey(frameworkName)) {
            Exporto_Log(frameworkName + " init");
            return true;
        }
        else if ([mocha loadFrameworkWithName:frameworkName inDirectory:directory]) {
            Exporto_Log(frameworkName + " init");
            mocha.setValue_forKey_(true, frameworkName);
            return true;
        }
        else {
            Exporto_Log("UNABLE TO LOAD FRAMEWORK >>> 🦊");
            return false;
        }
    })();
};
