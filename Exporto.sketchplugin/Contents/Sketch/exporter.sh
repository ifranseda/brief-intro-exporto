#!/bin/bash

# exporter
# Author: Isnan Franseda
# Exporto 2017

# sketchtool installed may used different version with sketchtool provided by Sketch.app version installed 
# if you find error "Failed to open document", you may need to re-install sketchtool based on Sketch.app version installed
# For example: /Application/Sketch.app/Contents/Resources/sketchtool/install.sh
#
# This gist purpose to provide single asset management which will be managed by designers
# to avoid discrepancies on assets naming caused by developers using non-standard naming convention provided.
#

SKETCH_FILE=""

if [[ $# -lt 1 ]]
then
  echo "Usage:"
  echo "assets-exporter <prefix> <sketch-file> <destination>"
  exit 1
fi

if [[ ! -f $2 ]]; then
  echo "File '$2' not found!"
  exit 1
fi

if ! loc="$(type -p "/Applications/Sketch.app/Contents/Resources/sketchtool/bin/sketchtool")" || [ -z "$loc" ]; then
  echo "Please install sketchtool"
  echo ""
  echo "Example (You may need root access to install this command, using 'sudo'):"
  echo "/Applications/Sketch.app/Contents/Resources/sketchtool/install.sh"
  echo ""
  exit 1
fi

SKETCHTOOL_PATH=/Applications/Sketch.app/Contents/Resources/sketchtool/bin/sketchtool

PREFIX=$(echo $1 | tr '[:upper:]' '[:lower:]')

SKETCH_FILE="$2"

PROJECT_DIR=$( cd "$(dirname "$SKETCH_FILE")" ; pwd -P )

OUTPUT_DIR=${3:-$PROJECT_DIR}

TMP_DIR="$OUTPUT_DIR"/"tmp"
IOS_TMP_DIR="$TMP_DIR"/"ios"
ANDROID_TMP_DIR="$TMP_DIR"/"android"

ASSETS_DIR="$OUTPUT_DIR"/"assets"
IOS_ASSET_DIR="$ASSETS_DIR"/"ios"/"Assets.xcassets"
ANDROID_ASSET_DIR="$ASSETS_DIR"/"android"

if [[ ! -z $4 ]]; then
  SHOW_OUTPUT=1
fi


function exportIOSAssets()
{
  cd "$PROJECT_DIR"

  $SKETCHTOOL_PATH export slices "$SKETCH_FILE" --output="$IOS_TMP_DIR" --formats="png" --scales="1" > "$TMP_DIR"/"output.txt"
  $SKETCHTOOL_PATH export slices "$SKETCH_FILE" --output="$IOS_TMP_DIR" --formats="png" --scales="2" > "$TMP_DIR"/"output.txt"
  $SKETCHTOOL_PATH export slices "$SKETCH_FILE" --output="$IOS_TMP_DIR" --formats="png" --scales="3" > "$TMP_DIR"/"output.txt"

  # create assets to XCode
  cd "$IOS_TMP_DIR"

  for file in *.png
      do
      
      if [[ $file == *"@"* ]]
      then
        fname=${file%%@*}
      else
        fname=${file%%.*}
      fi

      # create imageset file
      assets_name="$PREFIX"_"$fname".imageset
      icon_assets_dir="$IOS_ASSET_DIR"/"$assets_name"

      # copy imageset file to XCode
      mkdir -p "$icon_assets_dir"
      /bin/cp "$file" "$icon_assets_dir"/"$PREFIX"_"$file"
      
      createContentJsonWithPNGAssets "$PREFIX"_"$fname"
      /bin/cp Contents.json "$icon_assets_dir"/Contents.json

      if [[ $SHOW_OUTPUT == 1 ]]; then
        echo "$icon_assets_dir"
      fi
  done
}

function exportAndroidAssets()
{
  cd "$PROJECT_DIR"

  # echo "$PROJECT_DIR"
  # return

  $SKETCHTOOL_PATH export slices "$SKETCH_FILE" --output="$ANDROID_TMP_DIR/drawable-mdpi" --formats="png" --scales="1" > "$TMP_DIR"/"output.txt"
  $SKETCHTOOL_PATH export slices "$SKETCH_FILE" --output="$ANDROID_TMP_DIR/drawable-hdpi" --formats="png" --scales="1.5" > "$TMP_DIR"/"output.txt"
  $SKETCHTOOL_PATH export slices "$SKETCH_FILE" --output="$ANDROID_TMP_DIR/drawable-xhdpi" --formats="png" --scales="2" > "$TMP_DIR"/"output.txt"
  $SKETCHTOOL_PATH export slices "$SKETCH_FILE" --output="$ANDROID_TMP_DIR/drawable-xxhdpi" --formats="png" --scales="3" > "$TMP_DIR"/"output.txt"
  $SKETCHTOOL_PATH export slices "$SKETCH_FILE" --output="$ANDROID_TMP_DIR/drawable-xxxhdpi" --formats="png" --scales="4" > "$TMP_DIR"/"output.txt"

  cd "$ANDROID_TMP_DIR"

  for folder in *
  do
    for file in $folder/*.png
        do

        if [[ $file == *"@"* ]]
        then
          fname=${file%%@*}
        else
          fname=${file%%.*}
        fi
       
        icon_assets_dir="$ANDROID_ASSET_DIR"/"$folder"
        
        fname=${fname##*/}
        mkdir -p "$icon_assets_dir"
        /bin/cp "$file" "$icon_assets_dir"/"$PREFIX"_"$fname.png"

        if [[ $SHOW_OUTPUT == 1 ]]; then
          echo "$icon_assets_dir"/"$PREFIX"_"$fname.png"
        fi
    done
  done
}

function exportAssets()
{
    rm -fr "$ASSETS_DIR"/*

    # create temp directory to export assets
    if [ ! -d "$DIRECTORY" ]; then
      mkdir -p "$IOS_TMP_DIR"
      mkdir -p "$ANDROID_TMP_DIR"
    fi

    exportAndroidAssets
    exportIOSAssets

    cd "$PROJECT_DIR"
    rm -rf "$TMP_DIR"

    echo "Export assets completed!"
    exit 0
}

function createContentJsonWithPNGAssets()
{
cat << EOF > Contents.json
{
  "images" : [
    {
      "idiom" : "universal",
      "filename" : "$1.png",
      "scale" : "1x"
    },
    {
      "idiom" : "universal",
      "filename" : "$1@2x.png",
      "scale" : "2x"
    },
    {
      "idiom" : "universal",
      "filename" : "$1@3x.png",
      "scale" : "3x"
    }
  ],
  "info" : {
    "version" : 1,
    "author" : "Exporto"
  }
}
EOF
}


# Generate assets from sketch file and export
exportAssets
