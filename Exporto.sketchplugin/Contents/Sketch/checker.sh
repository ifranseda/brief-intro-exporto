#!/bin/bash

# sketchtool-checker
# Author: Isnan Franseda
# Exporto 2017

if ! loc="$(type -p "/usr/local/bin/sketchtool")" || [ -z "$loc" ]; then
  echo 0
  exit 1
fi

echo 1
exit 0
